#!/usr/bin/env perl6
use v6;

=begin pod
=head1 NAME

Term::termbox

=head1 DESCRIPTION

Implimentation of Termbox in rakudo

=head1 SYNOPSIS


=end pod

use NativeCall;

enum Term::color <Default Black Red Green Yellow Blue Magenta Cyan White>;

class cell is repr('CStruct'){
    has uint32 $.ch;
    has uint16 $.fg;
    has uint16 $.bg;
}

class event is repr('CStruct'){
    has uint8 $.type;
    has uint8 $.mod;
    has uint16 $.key;
    has uint32 $.ch;
    has int32 $.w;
    has int32 $.h;
    has int32 $.x;
    has int32 $.y;
}

class Term::termbox {
    has event $!lastevent .=new;
    has Term::color $!lastfg is default(Default) = Default;
    has Term::color $!lastbg is default(Default) = Default;

    sub tb_shutdown() is native('libtermbox.so') {}
    sub tb_init() returns int32 is native('libtermbox.so') {*}

    sub tb_width() returns int32 is native('libtermbox.so') {*}
    sub tb_height() returns int32 is native('libtermbox.so') {*}

    sub tb_clear() is native('libtermbox.so') {*}
    sub tb_present() is native('libtermbox.so') {*}

    sub tb_set_cursor(int32,int32) is native('libtermbox.so') {*}
    sub tb_change_cell(int32,int32,uint32,uint16,uint16) is native('libtermbox.so') {*}
    #sub tb_blit(int32,int32,int32,int32,CArray[cell] is rw) is native('libtermbox.so') {*}

    sub tb_peek_event(event,int32) returns int32 is native('libtermbox.so') {*}
    sub tb_poll_event(event) returns int32 is native('libtermbox.so') {*}
    #sub tb_select_input_mode(int32) returns int32 is native('libtermbox.so') {*}

    submethod BUILD() {
        tb_init();
    }

    method width() returns int32 {
        return tb_width();
    }

    method height() returns int32 {
        return tb_width();
    }

    method clear() {
        tb_clear();
    }

    method present() {
        tb_present();
    }

    method cursor(int32 $x, int32 $y) {
        tb_set_cursor($x,$y);
    }

    method put(int32 $x, int32 $y, Str $str, Term::color $fg=$!lastfg, Term::color $bg=$!lastfg){
        return False when 0>$x || $x>tb_width();
        return False when 0>$y || $y>tb_height();
        for $str.split('', :skip-empty).kv -> $i,$ch { # Idk if there is a better way to loop through a string please tell me if you know
            tb_change_cell($x+$i,$y,$ch.encode.read-int8(0),$fg,$bg); # read-int8 might also not be right but works?
        }
        return True;
    }

    # @TODO(renzix): Implement tb_blit properly idk how to use it properly and being in raku doesnt help

    # Waits a set amount of time for a event and returns Nil if no event was found
    method peek_millis(int32 $milliseconds) returns Any {
        given tb_peek_event($!lastevent,$milliseconds) {
            when 0 { return Nil }
            when $_ < 0 { fail "event" }
            default { return $!lastevent }
        }
    }
    method peek(int32 $seconds) returns Any {
        return self.peek_millis($seconds*1000);
    }

    # Waits until a event happens
    method poll() returns event {
        when tb_poll_event($!lastevent) <= 0 { fail "event" };
        return $!lastevent;
    }

    method shutdown() {
        tb_shutdown();
    }
}
