#!/usr/bin/env perl6

# @TODO(renzix): Make this actually good
use Term::termbox;
my $t = Term::termbox.new();
$t.put(5,5,"Hello World!!!", Black, White);
$t.cursor(2,2);
$t.present();
say $t.poll();
say $t.peek_millis(3000);
say $t.peek(5000);
say $t.poll();
sleep 2;
$t.shutdown();
