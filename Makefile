# ------------------------------------------------
#  # Generic Makefile

# Author: eragon9981@gmail.com
# # Date  : 2017-06-11
#
# # Changelog :
# #		 2017-06-11: stole from yanick.rochon@gmail.com
# # ------------------------------------------------
#
PC       = perl6

LIBDIR   = lib
TESTDIR  = t

test:
	@perl6 -I $(LIBDIR) $(TESTDIR)/*
